export let ascendingOrder = ((value,i)=>{
    let result = value.sort()
    return result
})

export let descendingOrder = ((value,i)=>{
    let result = value.sort().reverse()
    return result
})