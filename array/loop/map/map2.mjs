// let arr1 = [1,2,3]
// let result = arr1.map((value,i)=>{
//     return value*3
// })
// console.log(result)

// let arr1 = ["my", "name", "is"]
// let result = arr1.map((value,i)=>{
//     return value.toUpperCase()
// })
// console.log(result)

let arr1 = ["my", "name", "is"]
let result = arr1.map((value,i)=>{
    // return value.toUpperCase()+"N"
    return (value+"N").toUpperCase()
})
console.log(result)