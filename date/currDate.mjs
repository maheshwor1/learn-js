const currentDate = new Date();
const isoString = currentDate.toISOString();
console.log(isoString);

// ISO format yyyy-mm-ddThh:mm:ss
// 2024-01-05T10:36:21.134Z
// must have 4 digit in year and 2 digit other else

console.log(new Date().toLocaleString())
console.log(new Date().toLocaleTimeString())
console.log(new Date().toLocaleDateString())
