let bask1 = ["apple","banana","orange"]
let bask2 = ["grapes","pinapple"]

let bask3 = [...bask1,"a",...bask2] //... is spread operator

console.log(bask3)