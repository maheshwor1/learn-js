{
    let a = 3
    {
        let a= 6
        {
            a= 22
            console.log(a)
        }
        console.log(a)
    }
    console.log(a)
}

// while changing variable it searches in its block if not found it will look in parent block