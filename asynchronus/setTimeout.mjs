// setTimeout(()=>{
//     console.log("hello")
// }, 2000)

console.log("Start");

const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Async operation complete");
  }, 2000);
});

myPromise.then((result) => {
  console.log(result);
});

console.log("End");
